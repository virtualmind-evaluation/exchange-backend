using System.Globalization;
using System.Net;
using Exchange.Application;
using Exchange.Application.Rate.Currencies;
using Exchange.UnitTests.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using Shouldly;
using JsonException = System.Text.Json.JsonException;

namespace Exchange.UnitTests.Rate.Currencies;

public class DollarRateServiceTests
{
    [Fact]
    public async Task Should_return_rate_if_service_works()
    {
        const decimal buy = 300;
        const decimal sell = 330;
        var optionsMock = new Mock<IOptions<DollarRateOptions>>();
        optionsMock.Setup(x => x.Value).Returns(new DollarRateOptions
        {
            RateServiceUrl = "http://hello.com"
        });
        var clientFactoryMock = new Mock<IHttpClientFactory>();

        var delegatingHandlerStub = new DelegatingHandlerStub(ApiResult);
        var client = new HttpClient(delegatingHandlerStub);

        clientFactoryMock.Setup(f => f.CreateClient(It.IsAny<string>())).Returns(client);
        var logger = Mock.Of<ILogger<DollarRateService>>();
        var dollarRateService = new DollarRateService(optionsMock.Object, clientFactoryMock.Object, logger);
        var result = await dollarRateService.GetRate();

        result.Sell.ShouldBe(sell);
        result.Buy.ShouldBe(buy);
        return;

        Task<HttpResponseMessage> ApiResult(HttpRequestMessage request, CancellationToken cancellationToken) =>
            Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JsonConvert.SerializeObject(new[]
                {
                    buy.ToString(CultureInfo.InvariantCulture), sell.ToString(CultureInfo.InvariantCulture),
                    "Any random string here"
                }))
            });
    }

    [Fact]
    public async Task Should_log_error_and_throw_application_exception_if_service_did_not_send_success_status_code()
    {
        var optionsMock = new Mock<IOptions<DollarRateOptions>>();
        optionsMock.Setup(x => x.Value).Returns(new DollarRateOptions
        {
            RateServiceUrl = "http://hello.com"
        });
        var clientFactoryMock = new Mock<IHttpClientFactory>();

        var delegatingHandlerStub = new DelegatingHandlerStub(ApiResult);
        var client = new HttpClient(delegatingHandlerStub);

        clientFactoryMock.Setup(f => f.CreateClient(It.IsAny<string>())).Returns(client);
        var loggerMock = new Mock<ILogger<DollarRateService>>();
        var dollarRateService = new DollarRateService(optionsMock.Object, clientFactoryMock.Object, loggerMock.Object);
        await Should.ThrowAsync<ApplicationException>(async () => await dollarRateService.GetRate());
        loggerMock.Verify(logger => logger.Log(It.Is<LogLevel>(logLevel => logLevel == LogLevel.Error),
            It.Is<EventId>(eventId => eventId.Id == 0),
            It.Is<It.IsAnyType>((@object, @type) =>
                @object.ToString() ==
                $"Rate Service request is returning a non successfully status code current status code is {HttpStatusCode.BadRequest}" &&
                @type.Name == "FormattedLogValues"),
            It.IsAny<Exception>(),
            It.IsAny<Func<It.IsAnyType, Exception, string>>()!), Times.Once);
        return;

        Task<HttpResponseMessage> ApiResult(HttpRequestMessage request, CancellationToken cancellationToken) =>
            Task.FromResult(new HttpResponseMessage(HttpStatusCode.BadRequest));
    }

    [Fact]
    public async Task Should_log_error_and_throw_application_exception_if_service_did_not_return_expected_result()
    {
        var optionsMock = new Mock<IOptions<DollarRateOptions>>();
        optionsMock.Setup(x => x.Value).Returns(new DollarRateOptions
        {
            RateServiceUrl = "http://hello.com"
        });
        var clientFactoryMock = new Mock<IHttpClientFactory>();

        var delegatingHandlerStub = new DelegatingHandlerStub(ApiResult);
        var client = new HttpClient(delegatingHandlerStub);

        clientFactoryMock.Setup(f => f.CreateClient(It.IsAny<string>())).Returns(client);
        var loggerMock = new Mock<ILogger<DollarRateService>>();
        var dollarRateService = new DollarRateService(optionsMock.Object, clientFactoryMock.Object, loggerMock.Object);
        await Should.ThrowAsync<ApplicationException>(async () => await dollarRateService.GetRate());
        loggerMock.Verify(logger => logger.Log(It.Is<LogLevel>(logLevel => logLevel == LogLevel.Error),
            It.Is<EventId>(eventId => eventId.Id == 0),
            It.Is<It.IsAnyType>((@object, @type) =>
                @object.ToString() ==
                $"{nameof(JsonException)} Error deserializing response from Dollar API" &&
                @type.Name == "FormattedLogValues"),
            It.IsAny<Exception>(),
            It.IsAny<Func<It.IsAnyType, Exception, string>>()!), Times.Once);
        return;

        Task<HttpResponseMessage> ApiResult(HttpRequestMessage request, CancellationToken cancellationToken) =>
            Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    Buy = 1,
                    Sell = 2
                }))
            });
    }
}