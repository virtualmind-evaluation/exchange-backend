using Exchange.Application.Rate.Currencies;
using Exchange.Models;
using Moq;
using Shouldly;

namespace Exchange.UnitTests.Rate.Currencies;

public class RealRateServiceTests
{
    [Fact]
    public async Task RealRate_Should_be_quarter_of_dollar_rate()
    {
        const decimal buy = 4;
        const decimal sell = 2;
        var dollarRateServiceMock = new Mock<IDollarRateService>();
        dollarRateServiceMock.Setup(x => x.GetRate()).ReturnsAsync(() => new CurrencyRate(buy, sell));
        var realRateService = new RealRateService(dollarRateServiceMock.Object);
        var realRate = await realRateService.GetRate();
        realRate.Buy.ShouldBe(buy / 4);
        realRate.Sell.ShouldBe(sell / 4);
    }
}