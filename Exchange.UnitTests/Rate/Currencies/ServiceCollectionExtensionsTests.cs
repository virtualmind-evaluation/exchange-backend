using Exchange.Application.Rate.Currencies;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;

namespace Exchange.UnitTests.Rate.Currencies;

public class ServiceCollectionExtensionsTests
{
    [Fact]
    public void Should_have_one_ICurrencyRateService_per_currency_in_enum()
    {
        var services = new ServiceCollection();
        var configuration = new ConfigurationManager();
        services.AddCurrencyRateServices(configuration);
        var sp = services.BuildServiceProvider();
        var currencyRateServices = sp.GetRequiredService<IDictionary<CurrencyIsoCode, ICurrencyRateService>>();
        currencyRateServices.Keys.ToArray().ShouldBeEquivalentTo(Enum.GetValues<CurrencyIsoCode>());
    }
}