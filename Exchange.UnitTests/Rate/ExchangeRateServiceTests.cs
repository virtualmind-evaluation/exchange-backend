using Exchange.Application.Infrastructure;
using Exchange.Application.Rate;
using Exchange.Application.Rate.Currencies;
using Exchange.Models;
using Microsoft.Extensions.Logging;
using Moq;
using Shouldly;

namespace Exchange.UnitTests.Rate;

public class ExchangeRateServiceTests
{
    [Fact]
    public async Task GetRate_Should_return_expected_result()
    {
        var expectedResult = new CurrencyRate(100, 120);
        const CurrencyIsoCode currencyCode = CurrencyIsoCode.USD;
        var currencyRateServiceMock = new Mock<ICurrencyRateService>();
        currencyRateServiceMock.Setup(x => x.GetRate()).ReturnsAsync(expectedResult);
        var loggerMock = new Mock<ILogger<ExchangeRateService>>();
        var dictionary = new Dictionary<CurrencyIsoCode, ICurrencyRateService>
        {
            {CurrencyIsoCode.USD, currencyRateServiceMock.Object}
        };

        var service = new ExchangeRateService(dictionary, loggerMock.Object);
        var result = await service.GetRate(currencyCode.ToString());
        result.ShouldBe(expectedResult);
        currencyRateServiceMock.Verify(x => x.GetRate(), Times.Once);
    }
    
    [Fact]
    public async Task GetRate_Should_log_error_and_throw_exception_if_currency_do_not_exist()
    {
        var usdResult = new CurrencyRate(100, 120);
        const CurrencyIsoCode currencyCode = CurrencyIsoCode.USD;
        const string otherCode = "USD2";
        var currencyRateServiceMock = new Mock<ICurrencyRateService>();
        currencyRateServiceMock.Setup(x => x.GetRate()).ReturnsAsync(usdResult);
        var loggerMock = new Mock<ILogger<ExchangeRateService>>();
        var dictionary = new Dictionary<CurrencyIsoCode, ICurrencyRateService>
        {
            {CurrencyIsoCode.USD, currencyRateServiceMock.Object}
        };

        var service = new ExchangeRateService(dictionary, loggerMock.Object);
        await Should.ThrowAsync<UsageException>(() => service.GetRate(otherCode));
        
        currencyRateServiceMock.Verify(x => x.GetRate(), Times.Never);
        loggerMock.Verify(logger => logger.Log(It.Is<LogLevel>(logLevel => logLevel == LogLevel.Error),
            It.Is<EventId>(eventId => eventId.Id == 0),
            It.Is<It.IsAnyType>((@object, @type) =>
                @object.ToString() ==
                $"Invalid currency code {otherCode}" &&
                @type.Name == "FormattedLogValues"),
            It.IsAny<Exception>(),
            It.IsAny<Func<It.IsAnyType, Exception, string>>()!), Times.Once);
    }
}