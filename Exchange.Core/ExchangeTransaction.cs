using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Exchange.Domain;

public class ExchangeTransaction
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; init; }

    [Required]
    public Guid UserId { get; init; }

    [Required]
    public Currency Currency { get; init; } = null!;

    [Required]
    [Precision(10)]
    public decimal Amount { get; init; }

    [Required]
    public DateTime CreatedAt { get; init; } = DateTime.UtcNow;
}