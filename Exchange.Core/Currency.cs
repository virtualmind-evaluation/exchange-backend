﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exchange.Domain;

public class Currency
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }
    
    [Required]
    [MinLength(3)]
    [MaxLength(3)]
    public string IsoCode { get; set; }
    
    [Required]
    public string Name { get; set; }

    [Required]
    public decimal MonthlyTransactionLimit { get; set; }
}