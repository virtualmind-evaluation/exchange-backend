using Exchange.Domain.Data.ContextConfiguration;
using Microsoft.EntityFrameworkCore;

namespace Exchange.Domain.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions options) : base(options)
    {
    }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfiguration(new CurrencyContextConfiguration());
        builder.ApplyConfiguration(new ExchangeTransactionContextConfiguration());
    }

    public DbSet<Currency> Currencies { get; set; }
    public DbSet<ExchangeTransaction> ExchangeTransactions { get; set; }
}