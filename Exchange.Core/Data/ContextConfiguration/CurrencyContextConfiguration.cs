using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exchange.Domain.Data.ContextConfiguration;

public class CurrencyContextConfiguration : IEntityTypeConfiguration<Currency>
{
    public void Configure(EntityTypeBuilder<Currency> builder)
    {
        builder.HasIndex(x => x.Name).IsUnique();
        builder.HasIndex(x => x.IsoCode).IsUnique();
        
        builder.HasData(
            new Currency
            {
                Id = Guid.NewGuid(),
                Name = "Dollar",
                IsoCode = "USD",
                MonthlyTransactionLimit = 200
            },
            new Currency
            {
                Id = Guid.NewGuid(),
                Name = "Real",
                IsoCode = "BRL",
                MonthlyTransactionLimit = 300
            }
        );
    }
}