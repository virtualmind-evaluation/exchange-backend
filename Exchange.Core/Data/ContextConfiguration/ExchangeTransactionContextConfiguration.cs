using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exchange.Domain.Data.ContextConfiguration;

public class ExchangeTransactionContextConfiguration : IEntityTypeConfiguration<ExchangeTransaction>
{
    public void Configure(EntityTypeBuilder<ExchangeTransaction> builder)
    {
        builder.HasIndex(x => x.UserId);
    }
}