namespace Exchange.Models;

public record CurrencyRate(decimal Buy, decimal Sell);