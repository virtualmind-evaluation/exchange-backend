﻿namespace Exchange.Models;

public record PurchaseRequest
{
    private readonly string _currencyCode = null!;
    public Guid UserId { get; init; }
    public decimal Amount { get; init; }
    public string CurrencyCode
    {
        get => _currencyCode;
        init => _currencyCode = value.ToUpperInvariant();
    }
}