using Exchange.Application;
using Exchange.Application.Infrastructure;
using Exchange.Application.Purchase;
using Exchange.Application.Rate;
using Exchange.Application.Rate.Currencies;
using Exchange.Domain.Data;
using Hellang.Middleware.ProblemDetails;
using Microsoft.EntityFrameworkCore;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var services = builder.Services;

services.AddDbContext<ApplicationDbContext>(options =>
    options.UseNpgsql(configuration.GetConnectionString("ExchangeDb")));
// Add services to the container.

services.AddControllers(x => x.UseRoutePrefix("api"));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
services.AddEndpointsApiExplorer();
services.AddSwaggerGen();

services.Configure<RouteOptions>(options => { options.LowercaseUrls = true; });

services.AddScoped<IClock, DefaultClock>();
services.AddScoped<IExchangePurchaseAmountValidator, ExchangePurchaseAmountValidator>();
services.AddScoped<IExchangePurchaseService, ExchangePurchaseService>();

services.AddScoped<IExchangeRateService, ExchangeRateService>();
services.AddCurrencyRateServices(configuration);
services.AddCommonErrorHandling(builder.Environment.IsDevelopment());

builder.Host.UseSerilog((context, _, configuration) => configuration
    .ReadFrom.Configuration(context.Configuration));


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.UseProblemDetails();

using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    db.Database.Migrate();
}

app.Run();