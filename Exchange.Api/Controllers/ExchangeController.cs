using Exchange.Application;
using Exchange.Application.Purchase;
using Exchange.Application.Rate;
using Exchange.Models;
using Microsoft.AspNetCore.Mvc;

namespace Exchange.Api.Controllers;

[ApiController]
[Produces("application/json")]
[Route("[controller]")]
public class ExchangeController : ControllerBase
{
    private readonly IExchangeRateService _exchangeRateService;
    private readonly IExchangePurchaseService _exchangePurchaseService;

    public ExchangeController(IExchangeRateService exchangeRateService,
        IExchangePurchaseService exchangePurchaseService)
    {
        _exchangeRateService = exchangeRateService;
        _exchangePurchaseService = exchangePurchaseService;
    }

    [HttpGet("{currencyCode}")]
    public Task<CurrencyRate> GetRate(string currencyCode) =>
        _exchangeRateService.GetRate(currencyCode);

    [HttpPost]
    public Task<decimal> Purchase([FromBody] PurchaseRequest request) =>
        _exchangePurchaseService.Purchase(request);

}