## Considerations future improvements

- The Service was created separating the concepts related to Get rate and purchase. Rate is independent of the DB and the purchase solution uses the DB to persist available currencies and Transactions
- API can be changed to receive Enum instead of string to avoid manual validation
- Transactions are persisted and validated using UTC time (not local time). It means the validation check if the user exceeds the transaction limit during the current month based on UTC instead of user local time,  It should be fixed persisting the local time for the user ids and persisting and validating based on it instead of local time
- Regarding send the User id on the transaction endpoint of course it is not a good practice and the correct solution is use an authorization solution. Sending on each request the Authorization token (for instance a Bearer token) and based on the token obtain which is the user that is doing the operation.
- Unit tests can be added using a TestDb or a database in memory
- Change the service to retrieve the BRL is something straightforward it implies just change the ICurrencyRateService for Real currency (RealRateService)
- Add a new currency is easy. We need a new value on the enum. A new entry on the DB and implement ICurrencyRateService for the new currency
