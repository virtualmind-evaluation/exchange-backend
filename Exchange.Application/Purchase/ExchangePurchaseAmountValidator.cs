using Exchange.Application.Infrastructure;
using Exchange.Domain.Data;
using Microsoft.EntityFrameworkCore;

namespace Exchange.Application.Purchase;

public class ExchangePurchaseAmountValidator : IExchangePurchaseAmountValidator
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IClock _clock;

    public ExchangePurchaseAmountValidator(ApplicationDbContext dbContext, IClock clock)
    {
        _dbContext = dbContext;
        _clock = clock;
    }

    public async Task EnsureRequestDoesNotExceedTransactionLimits(Guid userId, decimal amount, string currencyCode)
    {
        var now = _clock.UtcNow;
        var month = now.Month;
        var year = now.Year;
        
        var purchasedAmount = await _dbContext.ExchangeTransactions
            .Where(x =>
                x.UserId == userId && x.Currency.IsoCode == currencyCode && x.CreatedAt.Year == year &&
                x.CreatedAt.Month == month)
            .Select(x => x.Amount)
            .SumAsync();

        var monthlyTransactionLimit = _dbContext.Currencies
            .Where(x => x.IsoCode == currencyCode)
            .Select(x => x.MonthlyTransactionLimit)
            .First();
        
        if (monthlyTransactionLimit > purchasedAmount + amount) return;

        throw new UsageException(
            $"""
             Amount of transaction exceeds monthly transaction limit. Values are: Transaction Amount = {amount}
             , quota = {monthlyTransactionLimit}, purchased amount = {purchasedAmount}
             """);
    }
}