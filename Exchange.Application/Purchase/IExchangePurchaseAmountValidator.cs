namespace Exchange.Application.Purchase;

public interface IExchangePurchaseAmountValidator
{
    public Task EnsureRequestDoesNotExceedTransactionLimits(Guid userId, decimal amount, string currencyCode);
}