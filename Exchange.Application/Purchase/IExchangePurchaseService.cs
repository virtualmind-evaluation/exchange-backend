using Exchange.Models;

namespace Exchange.Application.Purchase;

public interface IExchangePurchaseService
{
    Task<decimal> Purchase(PurchaseRequest purchaseRequest);
}