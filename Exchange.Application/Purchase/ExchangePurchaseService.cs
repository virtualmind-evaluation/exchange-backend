using Exchange.Application.Infrastructure;
using Exchange.Application.Rate;
using Exchange.Domain;
using Exchange.Domain.Data;
using Exchange.Models;

namespace Exchange.Application.Purchase;

public class ExchangePurchaseService : IExchangePurchaseService
{
    private readonly IExchangeRateService _exchangeRateService;
    private readonly IExchangePurchaseAmountValidator _purchaseAmountValidator;
    private readonly ApplicationDbContext _dbContext;
    private readonly IClock _clock;

    public ExchangePurchaseService(IExchangeRateService exchangeRateService,
        IExchangePurchaseAmountValidator purchaseAmountValidator, ApplicationDbContext dbContext, IClock clock)
    {
        _exchangeRateService = exchangeRateService;
        _purchaseAmountValidator = purchaseAmountValidator;
        _dbContext = dbContext;
        _clock = clock;
    }

    public async Task<decimal> Purchase(PurchaseRequest purchaseRequest)
    {
        var currencyCode = purchaseRequest.CurrencyCode;
        var userId = purchaseRequest.UserId;
        var rate = await _exchangeRateService.GetRate(currencyCode);
        var amountToPurchase = Math.Round(purchaseRequest.Amount / rate.Sell, 2);
        await _purchaseAmountValidator.EnsureRequestDoesNotExceedTransactionLimits(userId, amountToPurchase, currencyCode);

        var currency = _dbContext.Currencies.First(x => x.IsoCode == currencyCode);

        var transaction = new ExchangeTransaction
        {
            Currency = currency,
            Amount = amountToPurchase,
            UserId = userId,
            CreatedAt = _clock.UtcNow
        };

        await _dbContext.AddAsync(transaction);
        await _dbContext.SaveChangesAsync();
        return amountToPurchase;
    }
}