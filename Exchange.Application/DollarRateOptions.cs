namespace Exchange.Application;

public class DollarRateOptions
{
    public const string SectionName = "DollarRate";
    public string RateServiceUrl { get; init; } = null!;
}