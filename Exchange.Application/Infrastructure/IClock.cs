namespace Exchange.Application.Infrastructure;

public interface IClock
{
    DateTime UtcNow { get; }
}