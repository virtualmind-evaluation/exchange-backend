using Hellang.Middleware.ProblemDetails;

namespace Exchange.Application.Infrastructure;

public static class ProblemDetailsOptionsExtensions
{
    public static void MapCommonExceptions(this ProblemDetailsOptions options)
    {
        options.Map((Func<UsageException, Microsoft.AspNetCore.Mvc.ProblemDetails>) (ex => new Microsoft.AspNetCore.Mvc.ProblemDetails()
        {
            Type = "https://httpstatuses.com/400",
            Title = "Bad request",
            Status = new int?(400),
            Detail = ex.Message
        }));
    }
}