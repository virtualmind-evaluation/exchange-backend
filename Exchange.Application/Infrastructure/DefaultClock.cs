namespace Exchange.Application.Infrastructure;

public class DefaultClock : IClock
{
    public DateTime UtcNow => DateTime.UtcNow;
}