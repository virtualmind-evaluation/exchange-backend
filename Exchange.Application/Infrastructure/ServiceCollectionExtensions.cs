using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using ProblemDetailsOptions = Hellang.Middleware.ProblemDetails.ProblemDetailsOptions;

namespace Exchange.Application.Infrastructure;

public static class ServiceCollectionExtensions
{
    public static void AddCommonErrorHandling(
        this IServiceCollection services,
        bool includeExceptionDetails = false,
        Action<ProblemDetailsOptions>? configure = null)
    {
        var action = configure;
        services.AddProblemDetails((Action<ProblemDetailsOptions>) (options =>
        {
            options.IncludeExceptionDetails = (Func<HttpContext, Exception, bool>) ((ctx, ex) => includeExceptionDetails);
            options.MapCommonExceptions();
            if (action == null)
                return;
            action(options);
        }));
    }
}