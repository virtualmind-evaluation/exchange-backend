using Exchange.Models;

namespace Exchange.Application.Rate.Currencies;

public interface ICurrencyRateService
{
    CurrencyIsoCode CurrencyIsoCode { get; }
    public Task<CurrencyRate> GetRate();
}