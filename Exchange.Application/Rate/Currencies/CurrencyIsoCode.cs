namespace Exchange.Application.Rate.Currencies;

public enum CurrencyIsoCode
{
    USD,
    BRL
}