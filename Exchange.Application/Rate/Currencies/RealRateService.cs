using Exchange.Models;

namespace Exchange.Application.Rate.Currencies;

public class RealRateService : ICurrencyRateService
{
    private readonly IDollarRateService _dollarRateService;

    public RealRateService(IDollarRateService dollarRateService)
    {
        _dollarRateService = dollarRateService;
    }

    public CurrencyIsoCode CurrencyIsoCode => CurrencyIsoCode.BRL;

    public async Task<CurrencyRate> GetRate()
    {
        var dollarRate = await _dollarRateService.GetRate();
        return new CurrencyRate(dollarRate.Buy / 4, dollarRate.Sell / 4);
    }
}