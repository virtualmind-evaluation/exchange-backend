using Exchange.Models;

namespace Exchange.Application.Rate.Currencies;

public interface IDollarRateService
{
    Task<CurrencyRate> GetRate();
}