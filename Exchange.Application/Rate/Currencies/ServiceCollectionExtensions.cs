using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Exchange.Application.Rate.Currencies;

public static class ServiceCollectionExtensions
{
    public static void AddCurrencyRateServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<DollarRateOptions>(configuration.GetSection(DollarRateOptions.SectionName));
        services.AddHttpClient<IDollarRateService>();
        services.AddScoped<ICurrencyRateService, DollarRateService>();
        services.AddScoped<IDollarRateService, DollarRateService>();
        services.AddScoped<ICurrencyRateService, RealRateService>();
        services.AddScoped<IDictionary<CurrencyIsoCode, ICurrencyRateService>>(sp =>
            sp.GetServices<ICurrencyRateService>().ToDictionary(x => x.CurrencyIsoCode, x => x));
    }
}