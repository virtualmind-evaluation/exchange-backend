using System.Text.Json;
using Exchange.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Exchange.Application.Rate.Currencies;

public class DollarRateService : ICurrencyRateService, IDollarRateService
{
    private readonly IOptions<DollarRateOptions> _options;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ILogger<DollarRateService> _logger;

    public DollarRateService(IOptions<DollarRateOptions> options, IHttpClientFactory httpClientFactory,
        ILogger<DollarRateService> logger)
    {
        _options = options;
        _httpClientFactory = httpClientFactory;
        _logger = logger;
    }

    public CurrencyIsoCode CurrencyIsoCode => CurrencyIsoCode.USD;

    public async Task<CurrencyRate> GetRate()
    {
        var httpClient = _httpClientFactory.CreateClient();
        var response = await httpClient.GetAsync(new Uri(_options.Value.RateServiceUrl));

        try
        {
            response.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException ex)
        {
            _logger.LogError(ex, "Rate Service request is returning a non successfully status code current status code is {StatusCode}", response.StatusCode);
            throw GetConsumingDollarApiException(ex);
        }

        var (buy, sell) = await ParseBuyAndSellFromResponse(response);
        return new CurrencyRate(buy, sell);
    }
    
    private async Task<(decimal, decimal)> ParseBuyAndSellFromResponse(HttpResponseMessage response)
    {
        var items = await DeserializeApiResponse(response);

        if (items.Length < 2)
        {
            _logger.LogError("Dollar API response return less elements than expected, returned = {DollarAPIItems} Expected = 2", items.Length);
            throw GetConsumingDollarApiException();
        }

        var buy = ParsePriceToDecimal(items[0]);
        var sell = ParsePriceToDecimal(items[1]);
        return (buy, sell);
    }

    private async Task<string[]> DeserializeApiResponse(HttpResponseMessage response)
    {
        var stream = await response.Content.ReadAsStreamAsync();
      
        try
        {
            var elements = await JsonSerializer.DeserializeAsync<string[]>(stream);

            if (elements != null) return elements;
            
            _logger.LogError("Error deserializing response from Dollar API. result is null");
            throw GetConsumingDollarApiException();

        }
        catch (ArgumentException e)
        {
            _logger.LogError("{ExceptionTypeName} Error deserializing response from Dollar API", nameof(ArgumentException));
            throw GetConsumingDollarApiException(e);
        }
        catch (JsonException e)
        {
            _logger.LogError("{ExceptionTypeName} Error deserializing response from Dollar API", nameof(JsonException));
            throw GetConsumingDollarApiException(e);
        }
        catch (NotSupportedException e)
        {
            _logger.LogError("{ExceptionTypeName} Error deserializing response from Dollar API", nameof(NotSupportedException));
            throw GetConsumingDollarApiException(e);
        }
    }

    private decimal ParsePriceToDecimal(string item)
    {
        var canParse = decimal.TryParse(item, out var result);
        
        if (canParse) return result;
        
        _logger.LogError("Cannot parse Dollar API item result to decimal, item is = {DollarAPIPrice}", item);
        throw GetConsumingDollarApiException();
    }
    
    private static ApplicationException GetConsumingDollarApiException(Exception? baseException = null) 
        => new("Error consuming Dollar API, please try again later", baseException);
}