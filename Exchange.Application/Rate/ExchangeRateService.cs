using Exchange.Application.Infrastructure;
using Exchange.Application.Rate.Currencies;
using Exchange.Models;
using Microsoft.Extensions.Logging;

namespace Exchange.Application.Rate;

public class ExchangeRateService : IExchangeRateService
{
    private readonly IDictionary<CurrencyIsoCode, ICurrencyRateService> _currencyRateServices;
    private readonly ILogger<ExchangeRateService> _logger;

    public ExchangeRateService(IDictionary<CurrencyIsoCode, ICurrencyRateService> currencyRateServices, ILogger<ExchangeRateService> logger)
    {
        _currencyRateServices = currencyRateServices;
        _logger = logger;
    }

    public Task<CurrencyRate> GetRate(string currencyCode)
    {
        EnsueCurrencyCodeIsValid(currencyCode);
        var parsedCurrencyCode = Enum.Parse<CurrencyIsoCode>(currencyCode);
        return _currencyRateServices[parsedCurrencyCode].GetRate();
    }

    public void EnsueCurrencyCodeIsValid(string currencyCode)
    {
        var validCurrencies = Enum.GetValues<CurrencyIsoCode>().Select(x => x.ToString());

        if (validCurrencies.Contains(currencyCode)) return;

        _logger.LogError("Invalid currency code {CurrencyCode}", currencyCode);
        throw new UsageException($"Currency code {currencyCode} is not valid one");
    }
}