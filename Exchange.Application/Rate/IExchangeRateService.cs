using Exchange.Models;

namespace Exchange.Application.Rate;

public interface IExchangeRateService
{
    Task<CurrencyRate> GetRate(string currencyCode);
}